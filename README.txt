The Runs adds javascript to the Access Control page so make the
table headings follow the mouse cursor down the screen. It is
helpful for large access control grids where the headings are
easily forgotten as you scroll.